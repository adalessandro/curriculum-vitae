# curriculum-vitae

## Dependencies

Install the proper dependencies on your system. E.g. debian bookworm:
```bash
$ apt install -y latexmk texlive-lang-spanish texlive-bibtex-extra biber
```

## Make it!

```bash
$ make
```

## Gitlab CI

Download the latest artifacts from:
https://gitlab.com/adalessandro/curriculum-vitae/-/jobs/artifacts/master/download?job=build
